package yoktavian.studio.telkochart;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private LineChart mChart;
    private LineChart mChart2;
    private SeekBar mSeekBarX, mSeekBarY;
    private TextView tvX, tvY;
    LineDataSet dataSet1, dataSet2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

//        mChart = (LineChart) findViewById(R.id.chart);
//        mChart2 = (LineChart) findViewById(R.id.chart2);
//        SetChart1();
//        SetChart2();

    }

    public void SetChart1(){
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(100, 0));
        entries.add(new Entry(98, 1));
        entries.add(new Entry(99, 2));
        entries.add(new Entry(92, 3));
        dataSet1 = new LineDataSet(entries, "sektor1");
        dataSet1.setCircleColor(Color.BLUE);
        dataSet1.setColor(Color.BLUE);

        ArrayList<Entry> entries2 = new ArrayList<>();
        entries2.add(new Entry(91, 0));
        entries2.add(new Entry(93, 1));
        entries2.add(new Entry(95, 2));
        entries2.add(new Entry(98, 3));
        dataSet2 = new LineDataSet(entries2, "sektor2");
        dataSet2.setCircleColor(Color.GREEN);
        dataSet2.setColor(Color.GREEN);

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("January");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("April");

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(dataSet1);
        dataSets.add(dataSet2);
        LineData data = new LineData(labels, dataSets);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(true);
        mChart.setPinchZoom(true);
        mChart.setBackgroundColor(Color.WHITE);
        mChart.setData(data);
        mChart.setTouchEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDragEnabled(true);
        mChart.animateY(3000);
    }

    public void SetChart2(){
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(31, 0));
        entries.add(new Entry(38, 1));
        entries.add(new Entry(32, 2));
        entries.add(new Entry(36, 3));
        dataSet1 = new LineDataSet(entries, "sektor1");
        dataSet1.setCircleColor(Color.BLUE);
        dataSet1.setColor(Color.BLUE);
// Tes git
        ArrayList<Entry> entries2 = new ArrayList<>();
        entries2.add(new Entry(26, 0));
        entries2.add(new Entry(22, 1));
        entries2.add(new Entry(22, 2));
        entries2.add(new Entry(21, 3));
        dataSet2 = new LineDataSet(entries2, "sektor2");
        dataSet2.setCircleColor(Color.GREEN);
        dataSet2.setColor(Color.GREEN);

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("January");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("April");

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(dataSet1);
        dataSets.add(dataSet2);
        LineData data = new LineData(labels, dataSets);
        mChart2.setDrawGridBackground(false);
        mChart2.setHighlightPerDragEnabled(true);
        mChart2.setPinchZoom(true);
        mChart2.setBackgroundColor(Color.WHITE);
        mChart2.setData(data);mChart.setData(data);
        mChart2.setTouchEnabled(true);
        mChart2.setScaleEnabled(true);
        mChart2.setDragEnabled(true);
        mChart.animateY(3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
